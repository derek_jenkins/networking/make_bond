#!/bin/bash
# make_bond.sh: interactive script that creates bonded nics based on user-provided input.
# # # # #
# Error stuff
# ===========
E_NOT_ROOT=55
E_BADARGS=65
E_BAD_IP=75
E_UNKNOWN_INIT=85
# # # # #
# Variables
# =========
ZERO_ARGS=0
IF_BOND_0="/etc/sysconfig/network-scripts/ifcfg-bond0"
BOND_0_SHORT="ifcfg-bond0"
IF_BOND_1="/etc/sysconfig/network-scripts/ifcfg-bond1"
BOND_1_SHORT="ifcfg-bond1"
IF_EM_1="/etc/sysconfig/network-scripts/ifcfg-em1"
IF_EM_2="/etc/sysconfig/network-scripts/ifcfg-em2"
IF_EM_3="/etc/sysconfig/network-scripts/ifcfg-em3"
IF_EM_4="/etc/sysconfig/network-scripts/ifcfg-em4"
IF_BACKUP_DIR="/root/interface_backups/$(date +%s)/"
BOND_CONF="/etc/modprode.d/bonding.conf"
# # # # #
# Functions
# =========
exit_if_not_root() {
    [[ $UID == 0 ]] || {
        echo "You must be root to run this script. Exiting now."
        exit "${E_NOT_ROOT}"
    }
}
usage() {
    echo "Usage: $(basename "$0")"
    echo; echo "$(basename "$0") is an interactive script that creates a bonded nic."
    echo "Call the script with no arguments, then you will be prompted for input."
    echo "The script ends with an attempt to restart networking. If it does not "
    echo "recognize your init system, or if restarting results in error, you will"
    echo "need to restart networking manually."
    exit "${E_BADARGS}"
}
gather_if_0_bonds() {
    read -p "Type the IP for ${BOND_0_SHORT}: " IF_BOND_0_IP
    read -p "Type the netmask for ${BOND_0_SHORT}: " IF_BOND_0_NETMASK
    read -p "Type the gateway for ${BOND_0_SHORT}: " IF_BOND_0_GATEWAY
}
gather_if_1_bonds() {
    read -p "Type the IP for ${BOND_1_SHORT}: " IF_BOND_1_IP
    read -p "Type the netmask for ${BOND_1_SHORT}: " IF_BOND_1_NETMASK
}
verify_user_selected_IPs() {
    VERIFY_USER_ANSWERS=( "${IF_BOND_0_IP}" "${IF_BOND_0_NETMASK}" "${IF_BOND_0_GATEWAY}" "${IF_BOND_1_IP}" "${IF_BOND_1_NETMASK}" )
    for IP_ANSWER in "${VERIFY_USER_ANSWERS[@]}"; do
        if [[ "${IP_ANSWER}" =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
            OIFS=${IFS}; IFS='.'; OCTET=($IP_ANSWER); IFS=${OIFS}
            [[ "${OCTET[0]}" -le 255 && "${OCTET[1]}" -le 255 && "${OCTET[2]}" -le 255 && "${OCTET[3]}" -le 255 ]] && continue || {
            echo "It appears that we have received a bad IP value. Exiting script now; please run again."
            exit "${E_BAD_IP}"
        }
        else
            echo "It appears that we received a bad IP value. Exiting script now; please run again."
            exit "${E_BAD_IP}"
        fi
    done
}
gather_bond_IPs_from_user() {
    gather_if_0_bonds
    gather_if_1_bonds
    verify_user_selected_IPs
}
create_backup_dir() {
    [[ -d "${IF_BACKUP_DIR}" ]] || mkdir -p "${IF_BACKUP_DIR}"
}
backup_bond_interface_confs() {
    [[ -f "${IF_BOND_0}" ]] && cp "${IF_BOND_0}" "${IF_BACKUP_DIR}"
    [[ -f "${IF_BOND_1}" ]] && cp "${IF_BOND_1}" "${IF_BACKUP_DIR}"
}
write_if_0_bond() {
    echo -e "DEVICE=bond0\nONBOOT=yes\nBOOTPROTO=static\nIPADDR=${IF_BOND_0_IP}\nNETMASK=${IF_BOND_0_NETMASK}\nGATEWAY=${IF_BOND_0_GATEWAY}" > "${IF_BOND_0}"
    unset IF_BOND_0_IP IF_BOND_0_NETMASK IF_BOND_0_GATEWAY IF_BOND_0 BOND_0_SHORT
}
write_if_1_bond() {
    echo -e "DEVICE=bond0\nONBOOT=yes\nBOOTPROTO=static\nIPADDR=${IF_BOND_1_IP}\nNETMASK=${IF_BOND_1_NETMASK}" > "${IF_BOND_1}"
    unset IF_BOND_1_IP IF_BOND_1_NETMASK IF_BOND_1 BOND_1_SHORT
}
write_bonds() {
    write_if_0_bond
    write_if_1_bond
}
backup_em_interfaces() {
    [[ -f "${IF_EM_1}" ]] && cp "${IF_EM_1}" "${IF_BACKUP_DIR}" || echo "There is no ${IF_EM_1} to backup."
    [[ -f "${IF_EM_2}" ]] && cp "${IF_EM_2}" "${IF_BACKUP_DIR}" || echo "There is no ${IF_EM_2} to backup."
    [[ -f "${IF_EM_3}" ]] && cp "${IF_EM_3}" "${IF_BACKUP_DIR}" || echo "There is no ${IF_EM_3} to backup."
    [[ -f "${IF_EM_4}" ]] && cp "${IF_EM_4}" "${IF_BACKUP_DIR}" || echo "There is no ${IF_EM_4} to backup."
}
write_em_1() {
    echo -e "DEVICE=em1\nONBOOT=yes\nBOOTPROTO=none\nMASTER=bond1\nSLAVE=yes" > "${IF_EM_1}"
    unset IF_EM_1
}
write_em_2() {
    echo -e "DEVICE=em2\nONBOOT=yes\nBOOTPROTO=none\nMASTER=bond1\nSLAVE=yes" > "${IF_EM_2}"
    unset IF_EM_2
}
write_em_3() {
    echo -e "DEVICE=em3\nONBOOT=yes\nBOOTPROTO=none\nMASTER=bond0\nSLAVE=yes" > "${IF_EM_3}"
    unset IF_EM_3
}
write_em_4() {
    echo -e "DEVICE=em4\nONBOOT=yes\nBOOTPROTO=none\nMASTER=bond0\nSLAVE=yes" > "${IF_EM_4}"
    unset IF_EM_4
}
write_em_config_files() {
    write_em_1
    write_em_2
    write_em_3
    write_em_4
}
backup_bonding-conf() {
    [[ -f "${BOND_CONF}" ]] && cp "${BOND_CONF}" "${IF_BACKUP_DIR}"
}
write_bonding-conf() {
    echo -e "alias bond0 bonding\nalias bond1 bonding\noptions bond0 miimon=100 mode=4 lacp_rate=1\noptions bond1 miimon=100 mode=4 lacp_rate=1" > "${BOND_CONF}"
    unset BOND_CONF
}
restart_networking() {
    [[ -d /etc/systemd/system/ ]] && {
        echo "Restarting network"
        systemctl restart network.service
        exit $?
    }
    [[ -d /etc/init.d/ ]] && {
        echo "Restarting network"
        /etc/init.d/network restart
        exit $?
    }
    [[ -d /etc/init/ ]] && {
        echo "Restarting network"
        service Network restart
        exit $?
    }
    [[ ! -d /etc/systemd/system/ && ! -d /etc/init.d/ && ! -d /etc/init/ ]] && {
        echo "Please restart networking manually"
        exit "${E_UNKNOWN_INIT}"
    }
    unset E_UNKNOWN_INIT
}
# # # # #
# "Main"
# ======
# Display usage if user supplies args, then exit -->
exit_if_not_root
[[ $# -eq "$ZERO_ARGS" ]] || usage
gather_bond_IPs_from_user
create_backup_dir
backup_bond_interface_confs
write_bonds
backup_em_interfaces
write_em_config_files
backup_bonding-conf
write_bonding-conf
restart_networking
